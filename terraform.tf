terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.97.0"
    }
  }
}
provider "azurerm" {
  subscription_id = "16453daa-3d19-489b-afce-896bb00745a6"
  client_id       = "3fb0962c-490a-42ed-87b9-d0f48b808459"
  client_secret   = "Sl47Q~Vbu1JOVypCpflKQn_r2HBkIqWQWME08"
  tenant_id       = "6e900491-b93e-4554-bdf4-ed39b5ed0e07"
  features {}
}

resource "azurerm_resource_group" "Vishal-Terraform-rg" {
  name     = "Vishal-Terraform-rg"
  location = "centralindia"
}

resource "azurerm_virtual_network" "Vishal-Terraform-Network" {
  name                = "Terraform-Network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.Vishal-Terraform-rg.location
  resource_group_name = azurerm_resource_group.Vishal-Terraform-rg.name
}

resource "azurerm_subnet" "Terraform-Sub-Network" {
  name                 = "internal-subnet"
  resource_group_name  = azurerm_resource_group.Vishal-Terraform-rg.name
  virtual_network_name = azurerm_virtual_network.Vishal-Terraform-Network.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "Terraform-network-Interface" {
  name                = "Terraform-nic"
  location            = azurerm_resource_group.Vishal-Terraform-rg.location
  resource_group_name = azurerm_resource_group.Vishal-Terraform-rg.name
  
  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.Terraform-Sub-Network.id
    private_ip_address_allocation = "Dynamic"
  }
}
resource "azurerm_windows_virtual_machine" "Terraform-Mac" {
  name                = "Terraform-Mac"
  resource_group_name = azurerm_resource_group.Vishal-Terraform-rg.name
  location            = azurerm_resource_group.Vishal-Terraform-rg.location
  size                = "Standard_F1"
  admin_username      = "Vishal"
  admin_password      = "Vi$hal911957895"
  network_interface_ids = [
    azurerm_network_interface.Terraform-network-Interface.id,
  ]
  

os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
    source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
}
